package task6.gomelCourses.io;


import task6.gomelCourses.beans.*;

import java.util.List;


public interface InputProcessor {


    public Backpack getBackpack();

    public List<Thing> getThings();


}
