package task4.com.senlainc.gomelCourses.constants;


public final class Constants {

    public static final String NEXT = "\n";

    public static final String INPUT_MESSAGE_SENTENCE = "Введите предложение: ";
    public static final String INPUT_MESSAGE_EXAMPLE = "Введите подсчитываемое слово: ";
    public static final String OUTPUT_MESSAGE_RESULT_HEAD = NEXT + "Введенное слово в заданном тексте употребляется ";
    public static final String OUTPUT_MESSAGE_RESULT_TEIL = " раз." + NEXT;


    private Constants() {}
}
