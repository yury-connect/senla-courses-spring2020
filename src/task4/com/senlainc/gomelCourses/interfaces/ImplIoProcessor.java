package task4.com.senlainc.gomelCourses.interfaces;

public interface ImplIoProcessor {


    public String get(String message);

    public void out(String message);

    public void closeResource();

}
