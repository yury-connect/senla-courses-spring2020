package task2.com.senlainc.gomelCourses.constants;


public final class Constants {

    private static final String NEXT = "\n";

    public static final String INPUT_MESSAGE_FIRST = "введенное первое число : ";
    public static final String INPUT_MESSAGE_SECONDE = "введенное второе число : ";

    public static final String ERROR_MESSAGE = "Вы ввели некорректные данные. Программа завершена :(" + NEXT;

    public static final String SCM_OUT_MESSAGE = "наименьшее общее кратное : "; // SCM (NOK)   - наименьшее общее кратное
    public static final String GCD_OUT_MESSAGE = "наибольший общий делитель : "; // GCD (NOD)   - наибольший общий делитель


    private Constants() {}
}
