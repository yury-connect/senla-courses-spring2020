package task5.com.senlainc.gomelCourses.interfaces;

public interface ImplIoProcessor {


    public int get(String message);

    public void out(String message);

    public void closeResource();

}
