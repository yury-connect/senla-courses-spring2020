package task5.com.senlainc.gomelCourses.constants;


public final class Constants {

    public static final String NEXT = "\n";
    public static final int MAX_COUNT = 100;
    public static final String INPUT_MESSAGE = "Введите длинну последовательности от 0 до " + MAX_COUNT + " : ";
    public static final String OUTPUT_MESSAGE_NUM_CONT = NEXT + "Количество чисел в последовательности: ";
    public static final String OUTPUT_MESSAGE_NUM_SOURCE = NEXT + "Источник данных при формировании последовательности: ";
    public static final String OUTPUT_MESSAGE_SEQUENCE = NEXT + "Выводим сформированную последовательность: ";
    public static final String OUTPUT_MESSAGE_POLY = NEXT + "Количество палиндромомв в последовательности: ";
    public static final String OUTPUT_MESSAGE_LIST_POLY = NEXT + "Выводим найденные палиндромы: ";
    public static final String OUTPUT_MESSAGE_OUT_DELIMITER = "; ";
    public static final String OUTPUT_MESSAGE_ERROR = NEXT + "Введенное число находится за границами допустимого диапазона :( ";

    // SrcTxtFileNum
    public final static String FILE_NAME = "src/task5/data/nums.txt";
    public final static String DELIMITER_INPUT_FILE = ", ";

    private Constants() {}
}
