package task5.com.senlainc.gomelCourses.inputData;

import task5.com.senlainc.gomelCourses.interfaces.ImplInputData;


public class SrcJavaFileNum implements ImplInputData {


    @Override
    public int[] getData(int count) {
        int[] result = {
                55, 228, 777, 469, 879, 605, 874, 873, 267, 800, 822, 216, 745, 902, 53, 248, 589, 521, 334, 25, 617, 843, 139, 18, 989, 514, 88, 507, 304, 737, 742, 358, 964, 518, 826, 843, 123, 700, 716, 390, 500, 537, 606, 244, 439, 658, 491, 27, 178, 825, 52, 795, 667, 191, 812, 656, 705, 899, 163, 8, 636, 904, 365, 600, 422, 191, 443, 544, 890, 158, 933, 389, 694, 538, 632, 133, 196, 123, 160, 373, 947, 212, 167, 614, 402, 979, 270, 106, 878, 432, 114, 513, 336, 478, 113, 757, 668, 555, 300, 558
        };
        return result;
    }
}