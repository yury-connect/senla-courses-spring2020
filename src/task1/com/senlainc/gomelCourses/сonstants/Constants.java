package task1.com.senlainc.gomelCourses.сonstants;

public final class Constants {


    private static final String NEXT = "\n";

    public static final String EVEN_OR_ODD_TRUE = "введенное число четное;" + NEXT;
    public static final String EVEN_OR_ODD_FALSE = "введенное число НЕ четное;" + NEXT;

    public static final String SIMPLE_OR_COMPOUND_TRUE = "число простое;" + NEXT;
    public static final String SIMPLE_OR_COMPOUND_FALSE = "число составное;" + NEXT;

    public static final String INPUT_MESSAGE = "Введите число: ";
    public static final String OUTPUT_MESSAGE = NEXT + "Введено число: ";
    public static final String ERROR_MESSAGE= "Вы ввели некорректные данные. Программа завершена :(" + NEXT;


    private Constants() {}
}
