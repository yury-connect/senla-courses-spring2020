package task3.com.senlainc.gomelCourses.сonstants;

public final class Constants {


    public static final String NEXT = "\n";

    public static final String INPUT_MESSAGE = "Введите предложение: ";
    public static final String OUTPUT_MESSAGE_SRC = NEXT + "Введено следующее предложение: " + NEXT;
    public static final String OUTPUT_MESSAGE_NUM = NEXT + "Количество слов в предложении: " + NEXT;
    public static final String OUTPUT_MESSAGE_SORT = "выведем все слова предложения в отсортированном виде: " + NEXT;
    public static final String OUTPUT_MESSAGE_COMP_NAME = NEXT + "При сортировке ипспользован компаратор: ";
    public static final String OUTPUT_MESSAGE_UP = NEXT + "Сделаем первую букву каждого слова заглавной: " + NEXT;
    public static final String OUTPUT_MESSAGE_ERROR = NEXT + "Вы не ввели данные. Программа завершена :(";
    public static final String DELIMITER = " ";


    private Constants() {}
}
